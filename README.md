## replag-embed

A fork of the https://replag.toolforge.org/ tool that provides embeddable snippets
for other tools to use.

See <https://phabricator.wikimedia.org/T321640> for more details.

## License

The original replag tool is Copyright (c) 2015-2024 Wikimedia Foundation and
contributors under the terms of the MIT License.

replag-embed is (C) 2024 Kunal Mehta, under the AGPL v3 or later.
