// SPDX-License-Identifier: MIT AND AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 Kunal Mehta <legoktm@debian.org>
function ready(fn) {
    if (document.readyState !== 'loading') {
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
}

async function main() {
    let replagEmbed = document.getElementById('replag-embed');
    if (replagEmbed === null) {
        return;
    }
    let params = {
        // DEFAULT_THRESHOLD = 30
        "threshold": parseFloat(replagEmbed.getAttribute('data-threshold')) || 30,
    }
    if (replagEmbed.hasAttribute("data-wiki")) {
        params["wiki"] = replagEmbed.getAttribute("data-wiki");
    }
    if (replagEmbed.hasAttribute("data-fake")) {
        params["fake"] = parseFloat(replagEmbed.getAttribute("data-fake"));
    }
    let url = "https://replag-embed.toolforge.org/frame?"
    if (replagEmbed.hasAttribute("data-demo")) {
        url = "/frame?"
    }
    const response = await fetch(url + (new URLSearchParams(params)).toString());
    replagEmbed.innerHTML = await response.text();
}

ready(main)
