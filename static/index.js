// SPDX-License-Identifier: MIT AND AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 Kunal Mehta <legoktm@debian.org>
function ready(fn) {
    if (document.readyState !== 'loading') {
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
}

// Keep in sync with Rust `format_seconds`
function formatSeconds(seconds) {
    const totalSeconds = Math.floor(seconds);
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const secondsRemaining = totalSeconds % 60;
    return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${secondsRemaining.toString().padStart(2, '0')}`;
}

function main() {
    // Initialize tableSorter
    $( '#by-wiki' ).tablesorter( { sortList:[[2,1]] } );

    const events = new EventSource('/feed');
    events.addEventListener("message", (event) => {
        const data = JSON.parse(event.data);
        console.log(`[${Date.now()}] ${JSON.stringify(data)}`);
        document.querySelectorAll(`table[data-mode="${data.mode}"] tr[data-slice="${data.slice}"]`).forEach((row) => {
            row.querySelector(".lag").textContent = data.lag;
            row.querySelector(".time").textContent = formatSeconds(data.lag);
            if (data.lag > 0) {
                row.className = "lagged";
            } else {
                row.className = "";
            }
        });
        if (data.mode == "max") {
            // Re-sort the by-wiki table
            $('table[data-mode="max"]').trigger("update", [true]);
        }
    });
}

ready(main)
