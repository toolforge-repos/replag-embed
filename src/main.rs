// SPDX-License-Identifier: MIT AND AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2015-2024 Wikimedia Foundation and contributors
// SPDX-FileCopyrightText: 2024 Kunal Mehta <legoktm@debian.org>
use anyhow::{anyhow, Result};
use cached::proc_macro::{cached, once};
use rocket::{
    error,
    fairing::AdHoc,
    fs::FileServer,
    get,
    http::{hyper::header::ACCESS_CONTROL_ALLOW_ORIGIN, Header},
    launch,
    response::stream::{Event, EventStream},
    routes,
    shield::{Frame, Shield},
    State,
};
use rocket_dyn_templates::{
    tera::{self, Value},
    Template,
};
use rocket_healthz::Healthz;
use serde::Serialize;
use std::{cmp::Ordering, collections::HashMap, iter::zip, ops::Deref, time::Duration};
use tokio::{
    time::{self, MissedTickBehavior},
    try_join,
};
use toolforge::pool::{mysql_async::prelude::Queryable, WikiPool, ANALYTICS_CLUSTER, WEB_CLUSTER};

/// Display warning if the lag is greater than this
const DEFAULT_THRESHOLD: f64 = 30.0;
const SLICES: [u8; 8] = [1, 2, 3, 4, 5, 6, 7, 8];

#[derive(Clone)]
struct Pools {
    analytics: WikiPool,
    web: WikiPool,
}

/// Create a cached map that lets you look up a slice from URL or domain name
#[once(result = true, sync_writes = true, time = 86000)]
async fn lookup_map(pool: &WikiPool) -> Result<HashMap<String, u8>> {
    let mut conn = pool.connect("meta_p").await?;
    let rows: Vec<(String, String, String)> =
        conn.query("SELECT dbname, url, slice FROM wiki").await?;
    let mut map = HashMap::new();
    for (dbname, url, slice) in rows {
        // convert slice from "s6.labsdb" to 6
        let slice = slice
            .strip_prefix('s')
            .unwrap()
            .strip_suffix(".labsdb")
            .unwrap()
            .parse::<u8>()?;
        map.insert(dbname, slice);
        map.insert(url.trim_start_matches("https://").to_string(), slice);
    }

    Ok(map)
}

/// Get the current lag from the heartbeat table, cached
///
/// The cache is for 15 seconds for the slice
#[cached(
    key = "u8",
    convert = r#"{slice}"#,
    result = true,
    sync_writes = true,
    time = 15
)]
async fn replag(pool: &WikiPool, slice: u8) -> Result<f64> {
    let mut conn = pool.connect_to_slice(slice.into()).await?;
    let row: f64 = conn
        .query_first("SELECT lag FROM heartbeat_p.heartbeat")
        .await?
        .ok_or_else(|| anyhow!("no heartbeat row"))?;
    Ok(row)
}

#[derive(Serialize, Clone, Debug)]
struct FrameAnyTemplate {}

#[get("/frame")]
async fn frame_any(pools: &State<Pools>) -> Result<Template, &'static str> {
    match replag_any(&pools.web).await {
        Ok(()) => {
            // no replag!
            Err("")
        }
        Err(()) => Ok(Template::render("frame", FrameAnyTemplate {})),
    }
}

#[derive(Serialize, Clone, Debug)]
struct FrameTemplate {
    slice: u8,
    lag: f64,
}

#[get("/frame?<wiki>&<threshold>&<fake>")]
async fn frame_endpoint(
    pools: &State<Pools>,
    wiki: &str,
    threshold: Option<f64>,
    fake: Option<f64>,
) -> Result<Template, &'static str> {
    match frame(pools, wiki).await {
        Ok(mut frame) => {
            // if the "fake" parameter is used for demo purposes, set it as the lag
            if let Some(lag) = fake {
                frame.lag = lag;
            }
            if frame.lag > threshold.unwrap_or(DEFAULT_THRESHOLD) {
                Ok(Template::render("frame", frame))
            } else {
                Err("")
            }
        }
        Err(err) => {
            error!("frame for {wiki}: {err}");
            // return nothing if there's an error
            Err("")
        }
    }
}

#[once(sync_writes = true, time = 30)]
async fn replag_any(pool: &WikiPool) -> Result<(), ()> {
    try_join!(
        replag_any_impl(pool, 1),
        replag_any_impl(pool, 2),
        replag_any_impl(pool, 3),
        replag_any_impl(pool, 4),
        replag_any_impl(pool, 5),
        replag_any_impl(pool, 6),
        replag_any_impl(pool, 7),
        replag_any_impl(pool, 8),
    )
    .map(|_| ())
}

/// Wrapper around replag that abuses Result so we can use try_join()
/// to short circuit if there's replag. Err(()) means there's lag,
/// while Ok(()) means there's no lag or some error
async fn replag_any_impl(pool: &WikiPool, slice: u8) -> Result<(), ()> {
    match replag(pool, slice).await {
        Ok(lag) => {
            if lag > DEFAULT_THRESHOLD {
                Err(())
            } else {
                Ok(())
            }
        }
        Err(err) => {
            error!("replag on s{slice}: {err}");
            Ok(())
        }
    }
}

/// Generate the iframe data, cached for 30 seconds
// TODO: consider some dynamic caching expiry strategy based on amount of replag
#[cached(
    result = true,
    sync_writes = true,
    time = 30,
    key = "String",
    convert = r#"{wiki.to_string()}"#
)]
async fn frame(pool: &Pools, wiki: &str) -> Result<FrameTemplate> {
    let mapping = lookup_map(&pool.web).await?;
    let slice = *mapping
        .get(wiki)
        .ok_or_else(|| anyhow!("no slice for {}", wiki))?;
    let lags = try_join!(replag(&pool.analytics, slice), replag(&pool.web, slice))?;
    // find the max lag
    let lag = *[lags.0, lags.1]
        .iter()
        .max_by(|a, b| a.total_cmp(b))
        .unwrap();

    Ok(FrameTemplate { slice, lag })
}

/// Convert seconds to HH:MM:SS (tera filter)
// Keep in sync with JavaScript `formatSeconds`
fn format_seconds(value: &Value, _args: &HashMap<String, Value>) -> Result<Value, tera::Error> {
    let seconds = value.as_f64().unwrap();
    let total_seconds = seconds as u64;
    let hours = total_seconds / 3600;
    let minutes = (total_seconds % 3600) / 60;
    let seconds = total_seconds % 60;
    Ok(Value::String(format!(
        "{:02}:{:02}:{:02}",
        hours, minutes, seconds
    )))
}

#[derive(Serialize, Clone, Debug)]
struct DemoTemplate {}

#[get("/demo")]
fn demo() -> Template {
    Template::render("demo", DemoTemplate {})
}

#[derive(Serialize, Clone, Debug)]
struct IndexTemplate {
    clusters: ClusterLag,
    by_wiki: Vec<(String, f64)>,
    mapping: HashMap<String, u8>,
}

#[derive(Serialize, Clone, Debug)]
struct ClusterLag {
    analytics: Vec<f64>,
    web: Vec<f64>,
}

async fn all_lag(pool: &WikiPool) -> Result<Vec<f64>> {
    let mut threads = vec![];
    for slice in SLICES {
        let pool = pool.clone();
        threads.push(tokio::spawn(async move { replag(&pool, slice).await }));
    }

    let mut all_lag = vec![];
    for thread in threads {
        all_lag.push(thread.await??);
    }

    Ok(all_lag)
}

async fn cluster_lag(pools: &Pools) -> (Vec<f64>, Vec<f64>, Vec<f64>) {
    let (analytics_lag, web_lag) =
        try_join!(all_lag(&pools.analytics), all_lag(&pools.web)).unwrap();
    let max_lag: Vec<_> = zip(analytics_lag.iter(), web_lag.iter())
        .map(|(a, b)| **[a, b].iter().max_by(|a, b| a.total_cmp(b)).unwrap())
        .collect();
    (analytics_lag, web_lag, max_lag)
}

#[get("/")]
async fn index(pool: &State<Pools>) -> Template {
    let (analytics_lag, web_lag, max_lag) = cluster_lag(pool).await;
    let mut by_wiki = vec![];
    let mapping = lookup_map(&pool.web).await.unwrap();
    for (wiki, slice) in &mapping {
        if wiki.ends_with(".org") {
            continue;
        }
        by_wiki.push((wiki.to_string(), max_lag[*slice as usize - 1]));
    }
    by_wiki.sort_by(|a, b| {
        // sort by lag (descending), then by name (ascending)
        let order = b.1.total_cmp(&a.1);
        if order != Ordering::Equal {
            return order;
        }
        a.0.cmp(&b.0)
    });
    Template::render(
        "index",
        IndexTemplate {
            clusters: ClusterLag {
                analytics: analytics_lag,
                web: web_lag,
            },
            by_wiki,
            mapping,
        },
    )
}

#[derive(Serialize, Clone, Debug)]
struct FeedEvent {
    mode: &'static str,
    slice: u8,
    lag: f64,
}

/// Diff two sets of lags and return events representing the changed states
fn diff_lag(mode: &'static str, old: Vec<f64>, new: &[f64]) -> Vec<FeedEvent> {
    let mut events = vec![];
    for (i, (old, new)) in zip(&old, new.iter()).enumerate() {
        // slice is index + 1
        let slice = i + 1;
        if old != new {
            events.push(FeedEvent {
                mode,
                slice: slice as u8,
                lag: *new,
            });
        }
    }

    events
}

/// An EventSource feed that checks replag every 10 seconds. To
/// optimize, we only send events if it has changed.
#[get("/feed")]
fn feed(pools: &State<Pools>) -> EventStream![] {
    let pools = pools.deref().clone();
    EventStream! {
        let mut interval = time::interval(Duration::from_secs(10));
        interval.set_missed_tick_behavior(MissedTickBehavior::Delay);
        let (mut analytics_lag, mut web_lag, mut max_lag) = cluster_lag(&pools).await;
        loop {
            interval.tick().await;
            let (new_analytics_lag, new_web_lag, new_max_lag) = cluster_lag(&pools).await;
            let mut events = diff_lag("analytics", analytics_lag, &new_analytics_lag);
            events.extend(diff_lag("web", web_lag, &new_web_lag));
            events.extend(diff_lag("max", max_lag, &new_max_lag));
            for event in events {
                yield Event::json(&event);
            }

            analytics_lag = new_analytics_lag;
            web_lag = new_web_lag;
            max_lag = new_max_lag;

        }
    }
}

#[launch]
fn rocket() -> _ {
    // We want to allow framing
    let shield = Shield::default().disable::<Frame>();
    rocket::build()
        .manage(Pools {
            analytics: WikiPool::new(ANALYTICS_CLUSTER).unwrap(),
            web: WikiPool::new(WEB_CLUSTER).unwrap(),
        })
        .attach(Template::custom(|engines| {
            engines
                .tera
                .register_filter("format_seconds", format_seconds);
        }))
        .attach(shield)
        .attach(Healthz::fairing())
        .attach(AdHoc::on_response("CORS", |req, resp| {
            Box::pin(async move {
                if req.uri().path().as_str().starts_with("/frame") {
                    resp.set_header(Header::new(ACCESS_CONTROL_ALLOW_ORIGIN.as_str(), "*"));
                }
            })
        }))
        .mount("/static", FileServer::from("static"))
        .mount("/", routes![frame_endpoint, frame_any, demo, index, feed])
}
